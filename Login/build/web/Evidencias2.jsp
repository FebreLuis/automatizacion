<%-- 
    Document   : CartaCompro2
    Created on : 15-ene-2019, 19:57:27
    Author     : Febre
--%>

<%@page import="clases.Pdf"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
        <link rel="stylesheet" href="css/estiloEstudiante.css">
        <link href="https://fonts.googleapis.com/css?family=Charm" rel="stylesheet">
        <title>Subir-Modificar</title>
    </head>
    <body>
        
         <%
             Integer dato = 0;
            try {
                Pdf pdf = (Pdf) request.getAttribute("row");
                dato = pdf.getCodigopdf();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            boolean icono = false;
            try {
                icono = (Boolean) request.getAttribute("row2");
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        %>
        
        
        <div id="nivel">
        <h1 class="titulo">Universidad Técnica Particular de Loja</h1>   
        
        </div>
    
    <h1 class="titulo_estudi">Estudiantes</h1>
        
    <div class="usuario_p" method= "post" name ="userR">
      Usuario: ${sessionScope.usuario}
    </div>
    
    
 <ul class ="menu">
   
    <li><a href="estudiantes.jsp" name = "enlace1">Inicio</a></li>
    <li><a href="proyectos.jsp">Proyectos</a></li>
    <li><a href="CartaCompro1.jsp">Cartas Compromiso</a></li>
    <li><a href="Evidencias1.jsp">Evidencias</a></li>
    <li><a href="Informes1.jsp">Informes Finales</a></li>
    <li><a href="index.jsp">Salir</a></li>

 </ul>
    
    <center>
        <h1>Modificar Subir</h1>
    </center>
       

        <form name="formpdf" action="ControllerPdf2" method="post" enctype="multipart/form-data">
            <div class="datagrid">
                <table>
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Campo</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <td colspan="2">
                            </td>
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr>
                            <td>
                                <label for="id">Nombre del archivo: </label>
                            </td>  
                            <td>
                                 <input type="text" name="txtname" value="<c:out value="${row.nombrepdf}" />" />
                            </td>
                        </tr>
                        <tr class="alt">
                            <td>
                                <label for="id">Seleccionar PDF: *</label>
                                <%
                                    if (icono) {
                                %>
                                <a href="pdf2?id=<%out.print(dato); %>" target="_blank"> Ver Pdf</a>
                                <%
                                    } else {
                                        out.print("No hay Pdf");
                                    }
                                %>
                            </td>  
                            <td>
                                <input type="file" name="fichero" value="" class="btn"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center"><input type="submit" value="Enviar Archivo" name="submit" id="btn" class="btn"/></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
        <a href="Evidencias1.jsp">Regresar</a>
        
        <div id="footer">
      <h4>Sistema de Gestión de Practicas Pre-Profesionales </h4>
   </div>
    </body>
</html>
