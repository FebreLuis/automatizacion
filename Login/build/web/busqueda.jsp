<%-- 
    Document   : proyectos
    Created on : 05-ene-2019, 22:00:32
    Author     : Febre
--%>
<%@page import="servlet.Muestra"%>
<%@page import="controlador.Consultas"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="controlador.Conexion"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/tabla.css">
        <link rel="stylesheet" href="css/estiloEstudiante.css">

        <link href="https://fonts.googleapis.com/css?family=Charm" rel="stylesheet">
        <title>Busqueda</title>
    </head>
    <body>
        <%
            Conexion cone = new Conexion();
            PreparedStatement pre = null;
            ResultSet rs = null;

            // Creo un objeto de clase consultas para acceder a sus metodos
            Consultas objConsul = new Consultas();  

             HttpSession sesion = request.getSession();
        %>

        <div id="nivel">
            <h1 class="titulo">Universidad T�cnica Particular de Loja</h1>   

        </div>

        <h1 class="titulo_estudi">Buscador</h1>

        <p class="usuario_p" method= "post" name="userR" >
         Usuario: ${sessionScope.usuario}
        </p>

        <ul class ="menu">

            <li><a href="estudiantes.jsp" name = "enlace1">Inicio</a></li>
            <li><a href="proyectos.jsp">Proyectos</a></li>
            <li><a href="#">Cartas Compromiso</a></li>
            <li><a href="#">Evidencias</a></li>
            <li><a href="#">Informe Final</a></li>
            <li><a href="index.jsp">Salir</a></li>


        </ul>
    <center>
           <div class="main-container">
            <form style ="color: white" action="Externo.jsp" class="form_contact" method="post">
                <label>Buscar Tutor Externo</label>
                <input type="text" name="externo" placeholder="Ingrese texto">
                <input type="submit" value="Buscar" id="btnSearchExtern">

            </form>
                <br>
                <form style ="color: white" action="Academico.jsp" class="form_contact" method="post">
                <label>Buscar Tutor Academico</label>
                <input type="text" name="academico" placeholder="Ingrese texto">
                <input type="submit" value="Buscar" id="btnSearchAca">

            </form>
            <br>
            <form style ="color: white" action="Estudiante.jsp" class="form_contact" method="post">
                <label>Buscar Estudiante</label>
                <input type="text" name="estudiante" placeholder="Ingrese texto">
                <input type="submit" value="Buscar" id="btnSearchest">

            </form>
            <br>
            <form style ="color: white" action="obtenerProyectos.jsp" class="form_contact" method="post">
                <label>Buscar Proyecto o Nivel GP</label>
                <input type="text" name="proyecto" placeholder="Ingrese texto">
                <input type="submit" value="Buscar" id="btnSearchpro">

            </form>
        </div></center>



        <div id="footer">
            <h4>Sistema de Gesti�n de Practicas Pre-Profesionales </h4>
        </div>
        <script type="text/javascript" src="bus.js"></script>
    </body>
</html>      
