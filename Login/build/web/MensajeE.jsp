<%-- 
    Document   : MensajeE
    Created on : 09-ene-2019, 20:32:08
    Author     : Febre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="css/mensaje.css">
         <title>Mensaje Error</title>
    <body class="w3-container w3-auto">

        <h2>Confirmación</h2>


        <div class="w3-panel w3-pale-green w3-border">
            <h3>Mensaje</h3>
            <p>Su mensaje no se pudo enviar.</p>
        </div>

        
        <div class="w3-panel w3-red w3-display-container">
        <h3>Error!</h3>
        <p> <a href="proyectos.jsp"> Proyectos</a> </p>
        </div>

    </body>




</body>
</html>
