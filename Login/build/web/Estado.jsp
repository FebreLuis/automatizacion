<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head><!--Archivo Estado Proyectos para buscar por nombre de Empresas-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Buscador Proyectos</title>
        <link rel="stylesheet" href="css/estiloEmpresa2.css">
        <link href="https://fonts.googleapis.com/css?family=Charm" rel="stylesheet">
    </head>

    <body>
        <div id="nivel">
            <h1 class="titulo">Universidad Técnica Particular de Loja</h1>   
        </div>
        <h1 class="titulo_estudi">Empresas</h1>
        <ul class ="menu">
            <li><a href="EmpresasInicio.jsp" name = "enlace1">Inicio</a></li>
            <li><a href="empresas.jsp">Postulacion</a></li>
            <li><a href="Estado.jsp">Estado de Proyectos</a></li>
            <li><a href="index.jsp">Salir</a></li>
        </ul>
        <div class="container"><!--Establecemos un contact tipo form para los estilos-->
            <form id ="contact" method="post" action="SrvEmpresa">
                <h1>Nombre de la Empresa con Proyecto</h1><br>
                <table  width="289" border="2" class="datos_form">
                    <tr>
                        <td><input type ="text" name="nombreEmpresa" class="texto"></td><!--Se ingresara el nombre de la empresa-->
                        <td><input type ="submit" name="Buscar" value="Buscar Proyectos" class="texto"></td>
                    </tr>
                </table>
            </form><br>
        </div>
        <div id="footer">
            <h4>Sistema de Gestión de Practicas Pre-Profesionales </h4>
        </div>
    </body>
</html>
