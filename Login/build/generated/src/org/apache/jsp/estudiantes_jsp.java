package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class estudiantes_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"es\">\n");
      out.write("<head>\n");
      out.write("    <meta charset=\"UTF-8\">\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/estiloEstudiante.css\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/main.css\">\n");
      out.write("    <link href=\"https://fonts.googleapis.com/css?family=Charm\" rel=\"stylesheet\">\n");
      out.write("    \n");
      out.write("    <title>Modulo Estudiantes</title>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\n");
      out.write("    <div id=\"nivel\">\n");
      out.write("        <h1 class=\"titulo\">Universidad Técnica Particular de Loja</h1>   \n");
      out.write("        \n");
      out.write("    </div>\n");
      out.write("    \n");
      out.write("    <h1 class=\"titulo_estudi\">Estudiantes</h1>\n");
      out.write("        \n");
      out.write("    <div class=\"usuario_p\" method= \"post\" name =\"userR\">\n");
      out.write("      Usuario: ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${sessionScope.usuario}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\n");
      out.write("    </div>\n");
      out.write("    \n");
      out.write("    \n");
      out.write(" <ul class =\"menu\">\n");
      out.write("   \n");
      out.write("    <li><a href=\"estudiantes.jsp\" name = \"enlace1\">Inicio</a></li>\n");
      out.write("    <li><a href=\"proyectos.jsp\">Proyectos</a></li>\n");
      out.write("    <li><a href=\"CartaCompro1.jsp\">Cartas Compromiso</a></li>\n");
      out.write("    <li><a href=\"Evidencias1.jsp\">Evidencias</a></li>\n");
      out.write("    <li><a href=\"Informes1.jsp\">Informes Finales</a></li>\n");
      out.write("    <li><a href=\"index.jsp\">Salir</a></li>\n");
      out.write("\n");
      out.write(" </ul>\n");
      out.write("    \n");
      out.write("    <div class =\"uno\"> <img src=\"img/Captura.PNG\" class=\"img\" alt=\"xhdzy\" /> </div>\n");
      out.write("    <div class =\"dos\"> <img src=\"img/Captura2.PNG\" class=\"img\" alt=\"xhdzy\" /> </div>\n");
      out.write("    <div class =\"tres\"> <img src=\"img/Captura3.PNG\" class=\"img\" alt=\"xhdzy\" /> </div>\n");
      out.write("    \n");
      out.write("   \n");
      out.write("  \n");
      out.write("\n");
      out.write("    \n");
      out.write("    <div id=\"footer\">\n");
      out.write("          <h4>Sistema de Gestión de Practicas Pre-Profesionales </h4>\n");
      out.write("          <div class =\"\"> <img src=\"img/user-7.png\" class=\"img\" alt=\"xhdzy\" /> </div>\n");
      out.write("          <h2> Luis Febre</h2>\n");
      out.write("    </div>\n");
      out.write("    \n");
      out.write("    \n");
      out.write("</body>\n");
      out.write("</html>      \n");
      out.write("      \n");
      out.write("        \n");
      out.write("  \n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
