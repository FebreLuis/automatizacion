package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;

public final class empresas_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(1);
    _jspx_dependants.add("/conexion.jsp");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!--Este archivo es la ventana principal del modulo Empresas, aqui consta el formulario donde se ingresa datos acerca\n");
      out.write("del proyecto a postular\n");
      out.write("Para el archivo empresas sus estilos  estan definidos en el archivo estilo_empre.css\n");
      out.write("Para el archivo estado su css es estilo_buscador.css\n");
      out.write("-->\n");
      out.write("\n");
      out.write('\n');
      out.write('\n');

    Connection conex = null;
    Statement sql = null;
    try {
        Class.forName("com.mysql.jdbc.Driver");
        conex = (Connection)DriverManager.getConnection("jdbc:mysql://localhost:3306/utpl","root","");
        sql = conex.createStatement();
    } catch (Exception e) {
        out.print("Error en la conexion" + e);
    }

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>EMPRESAS</title\n");
      out.write("        <!--Establecemos la direccion donde estan los estilos-->\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/estilo_empre.css\">\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <!--Aplicamos jquery para la barra del menu-->\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/estiloEmpresa.css\">\n");
      out.write("        <link href=\"https://fonts.googleapis.com/css?family=Charm\" rel=\"stylesheet\">\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div id=\"nivel\">\n");
      out.write("            <h1 class=\"titulo\">Universidad Técnica Particular de Loja</h1>   \n");
      out.write("\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("        <h1 class=\"titulo_estudi\">Empresas</h1>\n");
      out.write("        \n");
      out.write("        <!--Creamos un menu para re direccionar a estado y al mismo archivo-->\n");
      out.write("        \n");
      out.write("        <ul class =\"menu\">\n");
      out.write("            <li><a href=\"EmpresasInicio.jsp\" name = \"enlace1\">Inicio</a></li>\n");
      out.write("            <li><a href=\"empresas.jsp\">Postulacion</a></li>\n");
      out.write("            <li><a href=\"Estado.jsp\">Estado de Proyectos</a></li>\n");
      out.write("            <li><a href=\"index.jsp\">Salir</a></li>\n");
      out.write("        </ul>       \n");
      out.write("        <!--Variables para almacenar lo del formulario y enviar ala base en la tabla empresas-->\n");
      out.write("        <div class=\"container\">      \n");
      out.write("            ");
  
                String nombre = request.getParameter("nombreEmpresa");
                String conve = request.getParameter("convenio");
                String nombreD = request.getParameter("nombreDependencia");
                String nombreE = request.getParameter("nombreEncargado");
                String correoE = request.getParameter("correoEncargado");
                String teleE = request.getParameter("telefonoEncargado");
                String nombreP = request.getParameter("nombreProyecto");
                String numE = request.getParameter("nroEstudiantes");
                String activiti = request.getParameter("actividades");
                if (nombre != null && conve!=null && nombreD != null && nombreE!=null && correoE!=null  && nombreP!=null && activiti!=null){
                    String qry = "insert into empresa(nombreEmpresa, convenio, nombreDependencia, nombreEncargado, correoEncargado, telefonoEncargado , nombreProyecto, nroEstudiantes, actividades) values('" + nombre + "','" + conve + "','" + nombreD + "','" + nombreE + "','" + correoE + "','"+teleE+"','" + nombreP + "','" + numE + "','" + activiti + "')";
                    
                    sql.executeUpdate(qry);
                    out.println("<h1>Datos Guardados Exitosamente</h1>");
                }else{
            
      out.write("\n");
      out.write("            <!--Formulario-->\n");
      out.write("            <form id=\"contact\" name=\"frEmp\" action=\"empresas.jsp\"  method=\"post\">\n");
      out.write("                <h1 align=\"center\">Solicitud estudiantes Prácticas Pre Profesionales\n");
      out.write("                    Sistemas Informáticos y Computación</h1><br><br>\n");
      out.write("                <h2>Estimadas Instituciones/Empresas/Dependencias,</h2><br>\n");
      out.write("                <h2 align=\"justify\">Reciban un cordial saludo, estamos pronto a iniciar un nuevo ciclo académico y \n");
      out.write("                    estaremos gustosos de antender sus requerimientos de estudiantes de la carrera de \n");
      out.write("                    Sistemas Informáticos y Computación de la Universidad Técnica Particular de Loja \n");
      out.write("                    para que apoyen en sus proyectos durante el periodo Abril 2019 - Agosto 2019  \n");
      out.write("                    en calidad de Practicantes o Pasantes. Para ello les pido muy comedidamente que ingresen \n");
      out.write("                    la información del siguiente formulario. \n");
      out.write("                    <br><br>Agradecemos su tiempo y apertura.</h2><br><br>\n");
      out.write("\n");
      out.write("                <label>Nombre de la Empresa/Institución</label>\n");
      out.write("                <input placeholder=\"Escriba su respuesta\" type=\"text\" name=\"nombreEmpresa\" tabindex=\"1\"  required><br><br>\n");
      out.write("\n");
      out.write("                <label>¿Mantiene su empresa un convenio con la carrera de Sistemas Informáticos y Computación de la UTPL? Si/No</label>\n");
      out.write("                <p>\n");
      out.write("                    <!--<input type=\"radio\" name=\"c\" value=\"Si\" >Si\n");
      out.write("                    <input type=\"radio\" name=\"c\" value=\"No\">No-->\n");
      out.write("                    <input placeholder=\"Escriba su respuesta\" type=\"text\" name=\"convenio\" maxlength=\"2\" tabindex=\"2\" required><br><br>\n");
      out.write("                </p>\n");
      out.write("                <br>\n");
      out.write("                <label>Nombre de la dependencia</label>\n");
      out.write("                <input placeholder=\"Escriba su respuesta\" type=\"text\" name=\"nombreDependencia\" tabindex=\"2\" required><br><br><br>\n");
      out.write("\n");
      out.write("                <label>Nombres y apellidos del encargado del estudiante</label>    \n");
      out.write("                <input placeholder=\"Escriba su respuesta\" type=\"text\" name=\"nombreEncargado\" tabindex=\"3\" required><br><br>\n");
      out.write("\n");
      out.write("                <label>E-mail del encargado del proyecto</label>\n");
      out.write("                <input placeholder=\"Escriba su respuesta\" type=\"email\" name=\"correoEncargado\" tabindex=\"4\" required><br><br>\n");
      out.write("\n");
      out.write("                <label>Contacto telefónico del encargado del estudiante</label><br>\n");
      out.write("                <input placeholder=\"Escriba su respuesta\" type=\"text\" maxlength=\"10\" name=\"telefonoEncargado\" tabindex=\"5\" ><br><br>\n");
      out.write("\n");
      out.write("                <label>Nombre del proyecto </label>\n");
      out.write("                <input placeholder=\"Escriba su respuesta\" type=\"text\" name=\"nombreProyecto\" tabindex=\"6\" required><br><br>\n");
      out.write("\n");
      out.write("                <label>Seleccione el número de estudiantes requeridos</label>\n");
      out.write("\n");
      out.write("                <input placeholder=\"Escriba su respuesta\" type=\"text\" maxlength=\"1\" name=\"nroEstudiantes\" tabindex=\"5\" ><br><br>\n");
      out.write("                \n");
      out.write("                <label>Por favor, indique las actividades a desarrollarse o entregables esperados del proyecto con un cronograma tentativo de abril 2019 a agosto 2019.<br> Por ejemplo:<br><br>\n");
      out.write("                    1. Levantamiento de requerimientos - Del 8 al 19 de abril<br>\n");
      out.write("                    2. Diseño de solución arquitectónica - Del 22 al 31 de abril</label><br><br>\n");
      out.write("                <textarea name=\"actividades\" placeholder=\"Escriba su respuesta\" tabindex=\"7\" rows=\"4\" cols=\"100\" ></textarea>\n");
      out.write("                <br>\n");
      out.write("                <!--Guardamos en la base de datos con el boton guardar-->\n");
      out.write("                <button type=\"submit\" value=\"Guardar\" id=\"contact-submit\" data-submit=\"...Sending\">Guardar</button>\n");
      out.write("            </form>\n");
      out.write("            ");
}
      out.write("\n");
      out.write("        </div>\n");
      out.write("        <div id=\"footer\">\n");
      out.write("            <h4>Sistema de Gestión de Practicas Pre-Profesionales </h4>\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
