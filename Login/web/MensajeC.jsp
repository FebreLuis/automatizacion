<%-- 
    Document   : MensajeC
    Created on : 09-ene-2019, 19:52:43
    Author     : Febre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <title>Mensaje Confirmación</title>
        <link rel="stylesheet" href="css/mensaje.css">
    <body class="w3-container w3-auto">

        <h2>Confirmación</h2>


        <div class="w3-panel w3-pale-green w3-border">
            <h3>Mensaje</h3>
            <p>Su mensaje ha sido enviado Corectamente.</p>
        </div>

        <div class="w3-panel w3-green">
            <h3>Volver!</h3>
            <p> <a href="proyectos.jsp"> Proyectos</a> </p>
        </div>

    </body>




</body>
</html>
