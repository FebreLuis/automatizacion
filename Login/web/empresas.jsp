<!--Este archivo es la ventana principal del modulo Empresas, aqui consta el formulario donde se ingresa datos acerca
del proyecto a postular
Para el archivo empresas sus estilos  estan definidos en el archivo estilo_empre.css
Para el archivo estado su css es estilo_buscador.css
-->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="conexion.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>EMPRESAS</title
        <!--Establecemos la direccion donde estan los estilos-->
        <link rel="stylesheet" type="text/css" href="css/estilo_empre.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!--Aplicamos jquery para la barra del menu-->
        <link rel="stylesheet" href="css/estiloEmpresa.css">
        <link href="https://fonts.googleapis.com/css?family=Charm" rel="stylesheet">
    </head>
    <body>
        <div id="nivel">
            <h1 class="titulo">Universidad Técnica Particular de Loja</h1>   

        </div>

        <h1 class="titulo_estudi">Empresas</h1>
        
        <!--Creamos un menu para re direccionar a estado y al mismo archivo-->
        
        <ul class ="menu">
            <li><a href="EmpresasInicio.jsp" name = "enlace1">Inicio</a></li>
            <li><a href="empresas.jsp">Postulacion</a></li>
            <li><a href="Estado.jsp">Estado de Proyectos</a></li>
            <li><a href="index.jsp">Salir</a></li>
        </ul>       
        <!--Variables para almacenar lo del formulario y enviar ala base en la tabla empresas-->
        <div class="container">      
            <%  
                String nombre = request.getParameter("nombreEmpresa");
                String conve = request.getParameter("convenio");
                String nombreD = request.getParameter("nombreDependencia");
                String nombreE = request.getParameter("nombreEncargado");
                String correoE = request.getParameter("correoEncargado");
                String teleE = request.getParameter("telefonoEncargado");
                String nombreP = request.getParameter("nombreProyecto");
                String numE = request.getParameter("nroEstudiantes");
                String activiti = request.getParameter("actividades");
                if (nombre != null && conve!=null && nombreD != null && nombreE!=null && correoE!=null  && nombreP!=null && activiti!=null){
                    String qry = "insert into empresa(nombreEmpresa, convenio, nombreDependencia, nombreEncargado, correoEncargado, telefonoEncargado , nombreProyecto, nroEstudiantes, actividades) values('" + nombre + "','" + conve + "','" + nombreD + "','" + nombreE + "','" + correoE + "','"+teleE+"','" + nombreP + "','" + numE + "','" + activiti + "')";
                    
                    sql.executeUpdate(qry);
                    out.println("<h1>Datos Guardados Exitosamente</h1>");
                }else{
            %>
            <!--Formulario-->
            <form id="contact" name="frEmp" action="empresas.jsp"  method="post">
                <h1 align="center">Solicitud estudiantes Prácticas Pre Profesionales
                    Sistemas Informáticos y Computación</h1><br><br>
                <h2>Estimadas Instituciones/Empresas/Dependencias,</h2><br>
                <h2 align="justify">Reciban un cordial saludo, estamos pronto a iniciar un nuevo ciclo académico y 
                    estaremos gustosos de antender sus requerimientos de estudiantes de la carrera de 
                    Sistemas Informáticos y Computación de la Universidad Técnica Particular de Loja 
                    para que apoyen en sus proyectos durante el periodo Abril 2019 - Agosto 2019  
                    en calidad de Practicantes o Pasantes. Para ello les pido muy comedidamente que ingresen 
                    la información del siguiente formulario. 
                    <br><br>Agradecemos su tiempo y apertura.</h2><br><br>

                <label>Nombre de la Empresa/Institución</label>
                <input placeholder="Escriba su respuesta" type="text" name="nombreEmpresa" tabindex="1"  required><br><br>

                <label>¿Mantiene su empresa un convenio con la carrera de Sistemas Informáticos y Computación de la UTPL? Si/No</label>
                <p>
                    <!--<input type="radio" name="c" value="Si" >Si
                    <input type="radio" name="c" value="No">No-->
                    <input placeholder="Escriba su respuesta" type="text" name="convenio" maxlength="2" tabindex="2" required><br><br>
                </p>
                <br>
                <label>Nombre de la dependencia</label>
                <input placeholder="Escriba su respuesta" type="text" name="nombreDependencia" tabindex="2" required><br><br><br>

                <label>Nombres y apellidos del encargado del estudiante</label>    
                <input placeholder="Escriba su respuesta" type="text" name="nombreEncargado" tabindex="3" required><br><br>

                <label>E-mail del encargado del proyecto</label>
                <input placeholder="Escriba su respuesta" type="email" name="correoEncargado" tabindex="4" required><br><br>

                <label>Contacto telefónico del encargado del estudiante</label><br>
                <input placeholder="Escriba su respuesta" type="text" maxlength="10" name="telefonoEncargado" tabindex="5" ><br><br>

                <label>Nombre del proyecto </label>
                <input placeholder="Escriba su respuesta" type="text" name="nombreProyecto" tabindex="6" required><br><br>

                <label>Seleccione el número de estudiantes requeridos</label>

                <input placeholder="Escriba su respuesta" type="text" maxlength="1" name="nroEstudiantes" tabindex="5" ><br><br>
                
                <label>Por favor, indique las actividades a desarrollarse o entregables esperados del proyecto con un cronograma tentativo de abril 2019 a agosto 2019.<br> Por ejemplo:<br><br>
                    1. Levantamiento de requerimientos - Del 8 al 19 de abril<br>
                    2. Diseño de solución arquitectónica - Del 22 al 31 de abril</label><br><br>
                <textarea name="actividades" placeholder="Escriba su respuesta" tabindex="7" rows="4" cols="100" ></textarea>
                <br>
                <!--Guardamos en la base de datos con el boton guardar-->
                <button type="submit" value="Guardar" id="contact-submit" data-submit="...Sending">Guardar</button>
            </form>
            <%}%>
        </div>
        <div id="footer">
            <h4>Sistema de Gestión de Practicas Pre-Profesionales </h4>
        </div>
    </body>
</html>
