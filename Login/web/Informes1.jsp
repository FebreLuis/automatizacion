<%-- 
    Document   : CartaCompro1
    Created on : 24-ene-2019, 19:57:12
    Author     : Febre
--%>

<%@page import="clases.Pdf"%>
<%@page import="controlador.Consultas"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
        <link rel="stylesheet" href="css/estiloEstudiante.css">
        <link href="https://fonts.googleapis.com/css?family=Charm" rel="stylesheet">
        <title>Carta Compromiso</title>
    </head>
    <body>
        
        <div id="nivel">
        <h1 class="titulo">Universidad Técnica Particular de Loja</h1>   
        
    </div>
    
    <h1 class="titulo_estudi">Estudiantes</h1>
        
    <div class="usuario_p" method= "post" name ="userR">
      Usuario: ${sessionScope.usuario}
    </div>
    
    
 <ul class ="menu">
   
    <li><a href="estudiantes.jsp" name = "enlace1">Inicio</a></li>
    <li><a href="proyectos.jsp">Proyectos</a></li>
    <li><a href="CartaCompro1.jsp">Cartas Compromiso</a></li>
    <li><a href="Evidencias1.jsp">Evidencias</a></li>
    <li><a href="Informes1.jsp">Informes Finales</a></li>
    <li><a href="index.jsp">Salir</a></li>

 </ul>
        
        
        
    <center>
        <h1>Informes</h1>
    </center>

    <%
        
        // Creo un objeto para usar metodo de usuario
        HttpSession sesion = request.getSession();

        String usuario = String.valueOf(sesion.getAttribute("usuario"));
        
        Consultas emp = new Consultas();
       
        // Consulto la gp del usuario
        String nivel_gp = emp.retornarGp(usuario);
        
        Pdf pdfvo = new Pdf();
        
        //Envio la gp del usuario para ejecutar la consulta
        ArrayList<Pdf> listar = emp.Listar_PdfVO_IF(nivel_gp);
    %>

    <div class="datagrid" method = "get">
        <table>
            <thead>
                <tr>
                    <th>Codigo</th>
                    <th>Nombre</th>
                    <th>Pdf</th>
                    <th></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="4">
                        <div id="paging">
                            <ul>
                                <li> <a href="#"> <span>Antes</span></a> </li>
                                <li> <a href="#" class="active"> <span>1</span></a> </li>
                                <li> <a href="#"> <span>2</span> </a> </li>
                                <li> <a href="#"> <span>3</span> </a> </li>
                                <li> <a href="#"> <span>Siguiente</span> </a> </li>
                            </ul>
                        </div>
                </tr>
            </tfoot>
            <tbody>
                <%if (listar.size() > 0) {
                        for (Pdf listar2 : listar) {
                            pdfvo = listar2;
                %>
                <tr>
                    <td><%=pdfvo.getCodigopdf()%></td>
                    <td><%=pdfvo.getNombrepdf()%></td>
                    <td>
                        <%
                            if (pdfvo.getArchivopdf2() != null) {
                        %>
                        <a href="pdf3?id=<%=pdfvo.getCodigopdf()%>" target="_blank"><img src="img/mpdf.png" title="pdf"/></a>
                            <%
                                } else {
                                    out.print("Vacio");
                                }
                            %>
                    </td>
                    <td >
                        <a id="mostrar" href="ControllerPdf3?action=insert&id=<%=pdfvo.getCodigopdf()%>"> <img src="img/Nuevo.png" title="Nuevo registro"/></a>          
                        <a href="ControllerPdf3?action=edit&id=<%=pdfvo.getCodigopdf()%>"> <img src="img/Modificar.png" title="Modificar"/></a>
                        <a href="ControllerPdf3?action=delete&id=<%=pdfvo.getCodigopdf()%>"> <img src="img/Eliminar.png" title="Eliminar"/></a>
                    </td>
                </tr>
                <%}
                    }%>
            </tbody>
        </table>
    </div>
            
   <div id="footer">
      <h4>Sistema de Gestión de Practicas Pre-Profesionales </h4>
   </div>
</body>
</html>
