<%-- 
    Document   : proyectos
    Created on : 05-ene-2019, 22:00:32
    Author     : Febre
--%>
<%@page import="java.util.List"%>
<%@page import="clases.Proyecto"%>
<%@page import="servlet.Muestra"%>
<%@page import="controlador.Consultas"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="controlador.Conexion"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/tabla.css">
        <link rel="stylesheet" href="css/estiloEstudiante.css">

        <link href="https://fonts.googleapis.com/css?family=Charm" rel="stylesheet">
        <title>Busqueda</title>
    </head>
    <body>
        <%
            Conexion cone = new Conexion();
            PreparedStatement pre = null;
            ResultSet rs = null;

            // Creo un objeto de clase consultas para acceder a sus metodos
            Consultas objConsul = new Consultas();  

             HttpSession sesion = request.getSession();
        %>

        <div id="nivel">
            <h1 class="titulo">Universidad T�cnica Particular de Loja</h1>   

        </div>

        <h1 class="titulo_estudi">Proyectos</h1>

        <p class="usuario_p" method= "post" name="userR" >
         Usuario: ${sessionScope.usuario}
        </p>

        <ul class ="menu">

            <li><a href="estudiantes.jsp" name = "enlace1">Inicio</a></li>
            <li><a href="proyectos.jsp">Proyectos</a></li>
            <li><a href="#">Cartas Compromiso</a></li>
            <li><a href="#">Evidencias</a></li>
            <li><a href="#">Informe Final</a></li>
            <li><a href="index.jsp">Salir</a></li>


        </ul>
        <div class="main-container">
           
            <center>   <table >
                <thead>
                    <tr>
                        
                        
                        <th>Nombre del Proyecto</th>
                        <th>Nivel GP</th>
                        <th>Tutor Academico</th>
                        <th>Tutor Externo</th>
                        <th>Estudiante</th>
                        <th>Estado</th>
                           
                    </tr>
                </thead>
                <tbody>   
                  
                    <%           
                        //Capturo y trasformo a string
                       // String usuario =String.valueOf(sesion.getAttribute("externo"));
                      
                        
                        String proyecto = request.getParameter("proyecto");
                        
                        if(proyecto != null){
                        String sql = "select * from proyectos where gp = '" +proyecto +"' OR nombre_proyecto like '%" +proyecto+"%';";
                        PreparedStatement ps = cone.getConnection().prepareStatement(sql);
                        rs = ps.executeQuery();

                        while (rs.next()) {
                           
                    %>   
                 <tr>
                        <td ><%=rs.getString("nombre_proyecto")%></td>
                        <td ><%=rs.getString("gp")%></td>
                        <td ><%=rs.getString("tutor_externo")%></td>
                        <td ><%=rs.getString("tutor_academico")%></td>
                        <td ><%=rs.getString("estudiante")%></td>
                        <td ><%=rs.getString("estado")%></td>
                        
                        
                    </tr>
                    <%
                        }
                      }
                    %>
                   
                </tbody>           
            </table>
       </center>
        </div>



        <div id="footer">
            <h4>Sistema de Gesti�n de Practicas Pre-Profesionales </h4>
        </div>
        <script type="text/javascript" src="bus.js"></script>
    </body>
</html>      
