<%-- 
    Document   : estudiantes
    Created on : 10-dic-2018, 22:04:14
    Author     : Febre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/estiloEstudiante.css">
    <link rel="stylesheet" href="css/main.css">
    <link href="https://fonts.googleapis.com/css?family=Charm" rel="stylesheet">
    
    <title>Modulo Estudiantes</title>
</head>
<body>

    <div id="nivel">
        <h1 class="titulo">Universidad Técnica Particular de Loja</h1>   
        
    </div>
    
    <h1 class="titulo_estudi">Estudiantes</h1>
        
    <div class="usuario_p" method= "post" name ="userR">
      Usuario: ${sessionScope.usuario}
    </div>
    
    
 <ul class ="menu">
   
    <li><a href="estudiantes.jsp" name = "enlace1">Inicio</a></li>
    <li><a href="proyectos.jsp">Proyectos</a></li>
    <li><a href="CartaCompro1.jsp">Cartas Compromiso</a></li>
    <li><a href="Evidencias1.jsp">Evidencias</a></li>
    <li><a href="Informes1.jsp">Informes Finales</a></li>
    <li><a href="index.jsp">Salir</a></li>

 </ul>
    
    <div class ="uno"> <img src="img/Captura.PNG" class="img" alt="xhdzy" /> </div>
    <div class ="dos"> <img src="img/Captura2.PNG" class="img" alt="xhdzy" /> </div>
    <div class ="tres"> <img src="img/Captura3.PNG" class="img" alt="xhdzy" /> </div>
    
   
  

    
    <div id="footer">
          
          <div class ="usu"> <img src="img/user-7.png" class="img" alt="xhdzy" /> </div>
          <a class = "d">Desarrollador: Luis Febre</a>
          
          <h4>Sistema de Gestión de Practicas Pre-Profesionales </h4>
    </div>
    
    
</body>
</html>      
      
        
  
