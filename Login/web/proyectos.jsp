<%-- 
    Document   : proyectos
    Created on : 05-ene-2019, 22:00:32
    Author     : Febre
--%>
<%@page import="servlet.Muestra"%>
<%@page import="controlador.Consultas"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="controlador.Conexion"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
        <link rel="stylesheet" href="css/estiloEstudiante.css">

        <link href="https://fonts.googleapis.com/css?family=Charm" rel="stylesheet">
        <title>Proyectos Ofertados</title>
    </head>
    <body>
        <%
            Conexion cone = new Conexion();
            PreparedStatement pre = null;
            ResultSet rs = null;

            // Creo un objeto de clase consultas para acceder a sus metodos
            Consultas objConsul = new Consultas();  

             HttpSession sesion = request.getSession();
        %>

        <div id="nivel">
            <h1 class="titulo">Universidad T�cnica Particular de Loja</h1>   

        </div>

        <h1 class="titulo_estudi">Estudiantes</h1>

        <p class="usuario_p" method= "post" name="userR" >
         Usuario: ${sessionScope.usuario}
        </p>

        <ul class ="menu">

            <li><a href="estudiantes.jsp" name = "enlace1">Inicio</a></li>
            <li><a href="proyectos.jsp">Proyectos</a></li>
            <li><a href="CartaCompro1.jsp">Cartas Compromiso</a></li>
            <li><a href="Evidencias1.jsp">Evidencias</a></li>
            <li><a href="Informes1.jsp">Informes Finales</a></li>
            <li><a href="index.jsp">Salir</a></li>


        </ul>
        
        <center>
        <h1>Proyectos Ofertados</h1>
    </center>
        <div class="datagrid">
            <table >
                <thead>
                    <tr>
                        <th class="left">ID</th>
                        <th>Nombre Empresa</th>
                        <th>Nombre de la Dependencia</th>
                        <th>Nombres del Encargado</th>
                        <th>E-mail</th>
                        <th>Telefono</th>
                        <th>Nombre del Proyecto</th>
                        <th>Actividades a Desarrollarse</th>
                        <th>GP</th>
                        <th>Estado</th>
                        <th>Acci�n</th>
                    </tr>
                </thead>
                <tbody>   
                  
                    <%           
                        //Capturo y trasformo a string
                        String usuario =String.valueOf(sesion.getAttribute("usuario"));
                        
                        String retorna_gp = objConsul.retornarGp(usuario);
                        
                        if(retorna_gp != null){
                        String sql = "select * from proyectos where nivel_gp = '" +retorna_gp+"';";
                        PreparedStatement ps = cone.getConnection().prepareStatement(sql);
                        rs = ps.executeQuery();

                        while (rs.next()) {
                    %>   
                    <tr>
                        <td ><%=rs.getInt("id_proyectos")%></td>
                        <td ><%=rs.getString("nombre_empresa")%></td>
                        <td ><%=rs.getString("nombre_de_dependencia")%></td>
                        <td ><%=rs.getString("nombre_del_encargado")%></td>
                        <td ><%=rs.getString("email")%></td>
                        <td ><%=rs.getString("telefono")%></td>
                        <td ><%=rs.getString("nombre_proyecto")%></td>
                        <td ><%=rs.getString("actividades")%></td>
                        <td ><%=rs.getString("nivel_gp")%></td>
                        <td ><%=rs.getString("estado")%></td>
                        <td ><a class="btn-default" href="Correo.jsp" role="button">Solicitar</a></td>
                    </tr>
                    <%
                        }
                      }
                    %>
                   
                </tbody>           
            </table>
        </div>



        <div id="footer">
            <h4>Sistema de Gesti�n de Practicas Pre-Profesionales </h4>
        </div>
    </body>
</html>      
