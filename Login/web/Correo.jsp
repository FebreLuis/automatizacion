<%-- 
    Document   : Correo
    Created on : 09-ene-2019, 19:32:00
    Author     : Febre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta charset="UTF-8">
        <title>Correo</title>
        <link rel="stylesheet" href="css/estiloscorreo.css">
        <link rel="stylesheet" href="css/font-awesome.css">
    </head>
    <body>
        
        <section class="form_wrap">
            <section class="contact_info">
                <section class="info_title">
                    <span class="fa fa-user-circle"></span>
                    <h2>UTPL</h2>
                </section>
                <section class="info_items">
                    <p><span class="fa fa-envelope"></span>universidad.utpl.loja@gmail.com</p>
                    <p><span class="fa fa-mobile"></span> 07370 1444</p>
                    <div class="usuario_p" method= "post" name ="userR">
                     Usuario: ${sessionScope.usuario}
                    </div>
                    
                </section>
            </section>
            <form action="mail"class="form_contact" method= "post">
                    
                <h2>Datos Mensaje</h2>
                <div class="user_info">                           
                    <label >Correo electronico Destinatario *</label>
                    <input type="email" id="correo" name="correo">

                    <label >Asunto *</label>
                    <input type="text" id="asunto" name="asunto">

                    <label >Mensaje *</label>
                    <textarea id="mensaje" name="mensaje" ></textarea>

                    <input type="submit" value="Enviar Mensaje" id="btnSend" >
                </div>
            </form>
        </section>
    </body>
</html>