
package clases;

/**
 *
 * @author Febre
 */
public class Estudiante {
    private int id_estudiante;
    private String cedula;
    private String nombre;
    private String apellido;
    private String nivel_g;
    private String correo;
    private String usuario;
    private String pass;

    // Constructor

    public Estudiante(int id_estudiante) {
        this.id_estudiante = id_estudiante;
    }
    
    
    // Metodos y Funciones
    public int getId_estudiante() {
        return id_estudiante;
    }

    public void setId_estudiante(int id_estudiante) {
        this.id_estudiante = id_estudiante;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNivel_g() {
        return nivel_g;
    }

    public void setNivel_g(String nivel_g) {
        this.nivel_g = nivel_g;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
    
    
    
    
}
