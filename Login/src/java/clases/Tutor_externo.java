/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Usuario
 */
public class Tutor_externo {
    
    private String tutor_externo;
    private String email;
    private String telefono;
    private String nombre_proyecto;
    private String gp;

    public Tutor_externo() {
    }

    public Tutor_externo(String tutor_externo, String email, String telefono, String nombre_proyecto, String gp) {
        this.tutor_externo = tutor_externo;
        this.email = email;
        this.telefono = telefono;
        this.nombre_proyecto = nombre_proyecto;
        this.gp = gp;
    }

    public String getTutor_externo() {
        return tutor_externo;
    }

    public void setTutor_externo(String tutor_externo) {
        this.tutor_externo = tutor_externo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNombre_proyecto() {
        return nombre_proyecto;
    }

    public void setNombre_proyecto(String nombre_proyecto) {
        this.nombre_proyecto = nombre_proyecto;
    }

    public String getGp() {
        return gp;
    }

    public void setGp(String gp) {
        this.gp = gp;
    }
    
    
    
}
