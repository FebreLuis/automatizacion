package clases;

import java.io.InputStream;

/**
 *
 * @author Febre
 */
public class Pdf {

    private int codigopdf;
    private String nombrepdf;
    InputStream archivopdf;
    private byte[] archivopdf2;
    private String nivel_gp;
    private int id_estudiantes;

    // Constructor
    public Pdf(int codigopdf, String name, byte[] archivo, String nivel_gp, int id_estudiantes) {
        this.codigopdf =codigopdf;
        this.nombrepdf = name;
        this.archivopdf2 = archivo;
        this.nivel_gp = nivel_gp;
        this.id_estudiantes = id_estudiantes;

    }

    public Pdf() {
        
    }

    // Metodos y funciones
    public int getCodigopdf() {
        return codigopdf;
    }

    public void setCodigopdf(int codigopdf) {
        this.codigopdf = codigopdf;
    }

    public String getNombrepdf() {
        return nombrepdf;
    }

    public void setNombrepdf(String nombrepdf) {
        this.nombrepdf = nombrepdf;
    }

    public InputStream getArchivopdf() {
        return archivopdf;
    }

    public void setArchivopdf(InputStream archivopdf) {
        this.archivopdf = archivopdf;
    }

    public byte[] getArchivopdf2() {
        return archivopdf2;
    }

    public void setArchivopdf2(byte[] archivopdf2) {
        this.archivopdf2 = archivopdf2;
    }

    public String getNivel_gp() {
        return nivel_gp;
    }

    public void setNivel_gp(String nivel_gp) {
        this.nivel_gp = nivel_gp;
    }

    public int getId_estudiantes() {
        return id_estudiantes;
    }

    public void setId_estudiantes(int id_estudiantes) {
        this.id_estudiantes = id_estudiantes;
    }

}
