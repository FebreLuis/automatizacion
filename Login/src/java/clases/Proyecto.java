package clases;

/**
 *
 * @author Febre
 */
public class Proyecto {

    private int id_proyectos;
    private String nombre_empresa;
    private String nombre_dependencia;
    private String nombre_encargado;
    private String email;
    private String telefono;
    private String nombre_proyecto;
    private String actividades;
private String gp;
    private String estado;

    //Constructor
    public Proyecto(int id_proyectos) {
        this.id_proyectos = id_proyectos;
    }

    // Metodos - Funciones
    public int getId_proyectos() {
        return id_proyectos;
    }

    public void setId_proyectos(int id_proyectos) {
        this.id_proyectos = id_proyectos;
    }

    public String getNombre_empresa() {
        return nombre_empresa;
    }

    public void setNombre_empresa(String nombre_empresa) {
        this.nombre_empresa = nombre_empresa;
    }

    public String getNombre_dependencia() {
        return nombre_dependencia;
    }

    public void setNombre_dependencia(String nombre_dependencia) {
        this.nombre_dependencia = nombre_dependencia;
    }

    public String getNombre_encargado() {
        return nombre_encargado;
    }

    public void setNombre_encargado(String nombre_encargado) {
        this.nombre_encargado = nombre_encargado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNombre_proyecto() {
        return nombre_proyecto;
    }

    public void setNombre_proyecto(String nombre_proyecto) {
        this.nombre_proyecto = nombre_proyecto;
    }

    public String getActividades() {
        return actividades;
    }

    public void setActividades(String actividades) {
        this.actividades = actividades;
    }

    public String getGp() {
        return gp;
    }

    public void setGp(String gp) {
        this.gp = gp;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Proyecto{" + "id_proyectos=" + id_proyectos + ", nombre_empresa=" + nombre_empresa + ", nombre_dependencia=" + nombre_dependencia + ", nombre_encargado=" + nombre_encargado + ", email=" + email + ", telefono=" + telefono + ", nombre_proyecto=" + nombre_proyecto + ", actividades=" + actividades + ", gp=" + gp + ", estado=" + estado + '}';
    }

}
