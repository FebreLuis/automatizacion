package servlet;

import clases.Correo;
import controlador.Consultas;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Febre
 */
public class Mail extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        // Creo metodo correo
        Correo objCorreo = new Correo();

        // Creo metodo consultas
        Consultas objConsultas = new Consultas();

        // Creo un objeto para usar metodo de usuario
        HttpSession sesion = request.getSession();

        // Para capturar mediante servlet
        String correo = request.getParameter("correo");
        String asunto = request.getParameter("asunto");
        String mensaje = request.getParameter("mensaje");

        String usuario = String.valueOf(sesion.getAttribute("usuario"));

        // Para recuperar mediante sql
        String retorna_gp = objConsultas.retornarGp(usuario);
        String retornaNombre = objConsultas.retornarNombre(usuario);
        String retornaApellido = objConsultas.retornarApellido(usuario);
        String retornaCorreo = objConsultas.retornarCorreo(usuario);
        
        String mensajeFinal = "Estudiante " + retornaNombre + " " + retornaApellido + " de la GP " + retorna_gp + " mensaje " + mensaje+" \n Su Correo "+ retornaCorreo;

        System.out.println("Usario " + usuario);

        if (objCorreo.SendMail(correo, asunto, mensajeFinal) == true) {
            response.sendRedirect("MensajeC.jsp");
        } else {
            response.sendRedirect("MensajeE.jsp");
        }

    }

 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
