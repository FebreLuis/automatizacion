
package servlet;

import clases.Pdf;
import controlador.Consultas;
import controlador.sql;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author Febre
 */
// Creo un objeto para usar metodo de usuario
@WebServlet(name = "ControllerPdf3", urlPatterns = {"/ControllerPdf3"})
@MultipartConfig(maxFileSize = 16177215)    // upload file's size up to 16MB

public class ControllerPdf3 extends HttpServlet {

    public static final String lIST_STUDENT = "/Informes1.jsp";
    public static final String INSERT_OR_EDIT = "/Informes2.jsp";

    String estado = null;
    Consultas pdfdao;
    int id_pdf = -1;

    public ControllerPdf3() {
        pdfdao = new Consultas();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String forward = "";
        String action = request.getParameter("action");

        if (action.equalsIgnoreCase("delete")) {
            forward = lIST_STUDENT;

            int studentId = Integer.parseInt(request.getParameter("id"));

            pdfdao.Eliminar_PdfVO_IF(studentId);
        }
        if (action.equalsIgnoreCase("edit")) {
            forward = INSERT_OR_EDIT;
            int studentIdM = Integer.parseInt(request.getParameter("id"));
            id_pdf = studentIdM;
            Pdf pdfvo = pdfdao.getPdfVOById_IF(studentIdM);
            request.setAttribute("row", pdfvo);
            boolean boo = false;
            if (pdfvo.getArchivopdf2() != null) {
                boo = true;
            }
            request.setAttribute("row2", boo);
            estado = "edit";
        } else if (action.equalsIgnoreCase("insert")) {
            forward = INSERT_OR_EDIT;
            estado = "insert";
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Variables
        String usuario = "";
        String retorna_gp = "";
        String retorna_id = "";

        // Creo un objeto para usar metodo de usuario
        HttpSession sesion = request.getSession();

        usuario = String.valueOf(sesion.getAttribute("usuario"));

        // Para recuperar mediante sql
        retorna_gp = pdfdao.retornarGp(usuario);
        retorna_id = pdfdao.retornarId_Estudiantes(usuario);

        Pdf pdfvo = new Pdf();
        sql auto = new sql();
        int nuevoid = auto.auto_increm("SELECT MAX(codigopdf) FROM informes;");

        pdfvo.setNivel_gp(retorna_gp);
        pdfvo.setId_estudiantes(Integer.parseInt(retorna_id));

        try {
            String name = request.getParameter("txtname");
            pdfvo.setNombrepdf(name);

        } catch (Exception ex) {
            System.out.println("nombre: " + ex.getMessage());
        }

        InputStream inputStream = null;
        try {
            Part filePart = request.getPart("fichero");
            if (filePart.getSize() > 0) {
                System.out.println("Name: " + filePart.getName());
                System.out.println("Tamaño: " + filePart.getSize());
                System.out.println("Tipo: " + filePart.getContentType());
                inputStream = filePart.getInputStream();
            }
        } catch (Exception ex) {
            System.out.println("fichero: " + ex.getMessage());
        }

        try {

            if (estado.equalsIgnoreCase("insert")) {
                pdfvo.setCodigopdf(nuevoid);

                System.out.println("Nuevo: " + nuevoid);
                System.out.println("GP: " + retorna_gp);
                System.out.println("ID: " + retorna_id);

                if (inputStream != null) {
                    pdfvo.setArchivopdf(inputStream);

                }
                pdfdao.Agregar_PdfVO_IF(pdfvo);
            } else {
                pdfvo.setCodigopdf(id_pdf);
                if (inputStream != null) {
                    pdfvo.setArchivopdf(inputStream);
                    pdfdao.Modificar_PdfVO_IF(pdfvo);
                } else {
                    pdfdao.Modificar_PdfVO2_IF(pdfvo);
                }
            }
        } catch (Exception ex) {
            System.out.println("textos: " + ex.getMessage());
        }

        RequestDispatcher view = request.getRequestDispatcher("/Informes1.jsp");
        view.forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
