
package servlet;

import controlador.Consultas;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Febre
 */
@WebServlet(name = "SrvEmpresa", urlPatterns = {"/SrvEmpresa"})
public class SrvEmpresa extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try{
            ResultSet res;
            Consultas acce = new Consultas();
            //borrardo 22
            int id_empresa = 0;
            String nombreEmpresa = "";
            String convenio = "";
            String nombreD = "";
            String encargado = "";
            String correoE = "";
            String telefonoE = "";
            String nombreP = "";
            String nroE = "";
            String actividades = "";
            String estado = "";
            
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Listado</title>");//faltan estilos
            out.println("<link rel=stylesheet href=css/estiloEmpresa3.css");
            out.println("<link href=https://fonts.googleapis.com/css?family=Charm rel=stylesheet");
            out.println("</head>"); 
            
            out.println("<body>");
            
            out.println("<div id=nivel>");
            out.println("<h1 class=titulo>Universidad Técnica Particular de Loja</h1>");
            out.println("</div>");
            out.println("<h1 class=titulo_estudi>Empresas</h1>");
            
            out.println("<ul class=menu>");
            out.println("<li><a href=EmpresasInicio.jsp name=enlace1>Inicio</a></li>");
            out.println("<li><a href=empresas.jsp>Volver a Postulacion</a></li>");
            out.println("<li><a href=Estado.jsp>Volver a Buscador</a></li>");
            out.println("<li><a href=index.jsp>Salir</a></li>");
            out.println("</ul>");
            out.println("<br><div>");//le quite la id container
            out.println("<form id =contact2>");
            
            out.println("<table>");
            out.println("<caption>LISTA DE PROYECTOS</caption>");
            
            out.println("<thead>");  
            out.println("<tr><th>ID</th><th>Nombre Empresa</th><th>Convenio</th><th>Dependencia</th><th>Encargado</th><th>Correo Encargado</th><th>Telefono Encargado</th><th>Proyecto</th><th>Nro. Estudiantes</th><th>Actividades</th><th>Estado</th></tr>");
            out.println("</thead>");
            
            
            res=acce.Listado();
            
            if ((request.getParameter("nombreEmpresa")!="")) {
                res=acce.BuscarporNombre(request.getParameter("nombreEmpresa"));
            }
            if ((request.getParameter("nombreEmpresa")=="")) {
                res=acce.Listado();
            }
            
            
            while(res.next()){
                id_empresa= res.getInt("id_empresa");
                nombreEmpresa= res.getString("nombreEmpresa");
                convenio= res.getString("convenio");
                nombreD= res.getString("nombreDependencia");
                encargado= res.getString("nombreEncargado");
                correoE = res.getString("correoEncargado");
                telefonoE = res.getString("telefonoEncargado");
                nombreP = res.getString("nombreProyecto");
                nroE = res.getString("nroEstudiantes");
                actividades = res.getString("actividades");
                estado = res.getString("estadoP");
                
                out.println("<tbody>");
                out.println("<tr><td>"+ id_empresa+ "</td><td>"+ nombreEmpresa+ "</td><td>"+ convenio+ "</td><td>"+nombreD+ "</td><td>"+encargado+"</td><td>"+correoE+"</td><td>"+telefonoE+"</td><td>"+nombreP+"</td><td>"+nroE+"</td><td>"+actividades+"</td><td>"+estado+"</td></tr>");
                out.println("</tbody>");
            }
            
            out.println("</table>");
            out.println("</form>");
            out.println("</div>");
            out.println("<div id=footer>");
            out.println("<h4>Sistema de Gestión de Practicas Pre-Profesionales </h4>");
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
            
        }catch(Exception ex){
            Logger.getLogger(SrvEmpresa.class.getName()).log(Level.SEVERE, null, ex);
            }
         
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
