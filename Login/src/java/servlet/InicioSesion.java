package servlet;

import controlador.Consultas;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Febre
 */
public class InicioSesion extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        

        PrintWriter out = response.getWriter();
        
        // Creo un objeto para usar metodo de usuario
        HttpSession sesion = request.getSession();
        
        /* Creamos un objeto que es donde se guarda informacion de servlet  y ponemos lo que tiene en name en los ()*/
        String usuario = request.getParameter("usuario");
        String contraseña = request.getParameter("pass");

        // Creamos un objeto de consultas donde esta nuestro sql
        Consultas objCon = new Consultas();

        // Variables
        boolean retorno1 = false;
        boolean retorno2 = false;
        boolean retorno3 = false;

        // Verifico a cual modulo ingresar
        if (objCon.autenticar(usuario, contraseña) == true) {
            retorno1 = true;
            
        } else if (objCon.autenticar2(usuario, contraseña) == true) {
            retorno2 = true;
            
        } else if (objCon.autenticar3(usuario, contraseña) == true) {
            retorno3 = true;
            
        }

        // Redirecciona
        if (retorno1 == true) {
            //  Capturo y envio el atributo de usuario
            sesion.setAttribute("usuario", usuario);
            
            
            // Envio al jsp de estudiante
            response.sendRedirect("estudiantes.jsp");
            
        } else if (retorno2 == true) {

            response.sendRedirect("tutores_academicos.jsp");
        } else if (retorno3 == true) {
            
            response.sendRedirect("tutores_externos.jsp");
        } else {
            response.sendRedirect("index.jsp");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
