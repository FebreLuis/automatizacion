package controlador;

import clases.Correo;
import clases.Estudiante;
import clases.Pdf;
import clases.Proyecto;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Febre
 */
public class Consultas extends Conexion {

    Conexion cone = new Conexion();

    // Consultar
    public boolean autenticar(String usuarioI, String passI) {
        PreparedStatement pre = null;
        ResultSet rs = null;
        try {
            String sentencia = "select * from estudiantes where usuario = ? and pass  = ? ";
            pre = getConnection().prepareStatement(sentencia);

            pre.setString(1, usuarioI);
            pre.setString(2, passI);
            rs = pre.executeQuery();

            // Si se ejecuta retorna verdadero
            if (rs.absolute(1)) {
                return true;

            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return false;
    }

    public boolean autenticar2(String usuarioI, String passI) {
        PreparedStatement pre = null;
        ResultSet rs = null;
        try {
            String sentencia = "select * from tutor_academico where usuario = ? and pass  = ? ";
            pre = getConnection().prepareStatement(sentencia);

            pre.setString(1, usuarioI);
            pre.setString(2, passI);
            rs = pre.executeQuery();

            // Si se ejecuta retorna verdadero
            if (rs.absolute(1)) {
                return true;

            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return false;
    }

    public boolean autenticar3(String usuarioI, String passI) {
        PreparedStatement pre = null;
        ResultSet rs = null;
        try {
            String sentencia = "select * from tutor_externo where usuario = ? and pass  = ? ";
            pre = getConnection().prepareStatement(sentencia);

            pre.setString(1, usuarioI);
            pre.setString(2, passI);
            rs = pre.executeQuery();

            // Si se ejecuta retorna verdadero
            if (rs.absolute(1)) {
                return true;

            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return false;
    }

    public String retornarGp(String usuario) {
        String respuesta = null;
        String sql = "select nivel_gp from estudiantes where usuario = '" + usuario + "';";
        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            // Recorre hasta que encuentre el valor si existe
            while (rs.next()) {
                respuesta = String.valueOf(rs.getString("nivel_gp"));
            }

        } catch (Exception e) {
            System.err.println(e);
        }

        return respuesta;
    }

    public String retornarId_Estudiantes(String usuario) {
        String respuesta = null;
        String sql = "select id_estudiantes from estudiantes where usuario = '" + usuario + "';";
        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            // Recorre hasta que encuentre el valor si existe
            while (rs.next()) {
                respuesta = String.valueOf(rs.getString("id_estudiantes"));
            }

        } catch (Exception e) {
            System.err.println(e);
        }

        return respuesta;
    }

    public String retornarNombre(String usuario) {
        String respuesta = null;
        String sql = "select nombre from estudiantes where usuario = '" + usuario + "';";
        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            // Recorre hasta que encuentre el valor si existe
            while (rs.next()) {
                respuesta = String.valueOf(rs.getString("nombre"));
            }

        } catch (Exception e) {
            System.err.println(e);
        }

        return respuesta;
    }

    public String retornarApellido(String usuario) {
        String respuesta = null;
        String sql = "select apellido from estudiantes where usuario = '" + usuario + "';";
        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            // Recorre hasta que encuentre el valor si existe
            while (rs.next()) {
                respuesta = String.valueOf(rs.getString("apellido"));
            }

        } catch (Exception e) {
            System.err.println(e);
        }

        return respuesta;
    }

    public String retornarCorreo(String usuario) {
        String respuesta = null;
        String sql = "select correo from estudiantes where usuario = '" + usuario + "';";
        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            // Recorre hasta que encuentre el valor si existe
            while (rs.next()) {
                respuesta = String.valueOf(rs.getString("correo"));
            }

        } catch (Exception e) {
            System.err.println(e);
        }

        return respuesta;
    }

    public List<Proyecto> muestraProyectos(String gp) {
        String sql = "select * from proyectos where nivel_gp = '" + gp + "';";
        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Proyecto> list = new LinkedList<>();
            Proyecto objProyecto;

            while (rs.next()) {
                objProyecto = new Proyecto(rs.getInt("id_proyectos"));
                objProyecto.setNombre_empresa(rs.getString("nombre_empresa"));
                objProyecto.setNombre_dependencia(rs.getString("nombre_de_dependencia"));
                objProyecto.setNombre_encargado(rs.getString("nombre_del_encargado"));
                objProyecto.setEmail(rs.getString("email"));
                objProyecto.setTelefono(rs.getString("telefono"));
                objProyecto.setNombre_proyecto(rs.getString("nombre_proyecto"));
                objProyecto.setActividades(rs.getString("actividades"));
                objProyecto.setGp(rs.getString("nivel_gp"));
                objProyecto.setEstado(rs.getString("estado"));

                list.add(objProyecto);

            }
            return list;
        } catch (Exception e) {
            System.err.println(e);
            return null;
        }
    }// Fin

    // PARA  LOS PDF
    /*Metodo listar*/
    public ArrayList<Pdf> Listar_PdfVO(String nivel_gp) {
        ArrayList<Pdf> list = new ArrayList<Pdf>();

        //String sql = "SELECT * FROM pdf;";
        String sql = "SELECT cartas_compromiso.codigopdf, cartas_compromiso.nombrepdf, cartas_compromiso.archivopdf from cartas_compromiso, estudiantes where (estudiantes.id_estudiantes = cartas_compromiso.id_estudiantes) and (estudiantes.nivel_gp = cartas_compromiso.nivel_gp) and cartas_compromiso.nivel_gp = '" + nivel_gp + "';";

        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Pdf objPdf = new Pdf();
                objPdf.setCodigopdf(rs.getInt(1));
                objPdf.setNombrepdf(rs.getString(2));
                objPdf.setArchivopdf2(rs.getBytes(3));
                list.add(objPdf);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }


    /*Metodo agregar*/
    public void Agregar_PdfVO(Pdf objPdf) {

        String sql = "INSERT INTO cartas_compromiso (codigopdf, nombrepdf, archivopdf, nivel_gp, id_estudiantes) VALUES(?, ?, ?, ? ,?);";

        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);

            ps.setInt(1, objPdf.getCodigopdf());
            ps.setString(2, objPdf.getNombrepdf());
            ps.setBlob(3, objPdf.getArchivopdf());
            ps.setString(4, objPdf.getNivel_gp());
            ps.setInt(5, objPdf.getId_estudiantes());
            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }


    /*Metodo Modificar*/
    public void Modificar_PdfVO(Pdf objPdf) {

        String sql = "UPDATE cartas_compromiso SET nombrepdf = ?, archivopdf = ? WHERE codigopdf = ?;";

        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);

            ps.setString(1, objPdf.getNombrepdf());
            ps.setBlob(2, objPdf.getArchivopdf());
            ps.setInt(3, objPdf.getCodigopdf());
            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /*Metodo Modificar*/
    public void Modificar_PdfVO2(Pdf objPdf) {

        String sql = "UPDATE cartas_compromiso SET nombrepdf = ? WHERE codigopdf = ?;";

        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);

            ps.setString(1, objPdf.getNombrepdf());
            ps.setInt(2, objPdf.getCodigopdf());
            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /*Metodo Eliminar*/
    public void Eliminar_PdfVO(int id) {

        String sql = "DELETE FROM cartas_compromiso WHERE codigopdf = ?;";

        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);

            ps.setInt(1, id);
            ps.executeUpdate();

        } catch (Exception ex) {
            System.err.println(ex);
        }
    }


    /*Metodo Consulta id*/
    public Pdf getPdfVOById(int studentId) {
        Pdf ObjPdf = new Pdf();
        String sql = "SELECT * FROM cartas_compromiso WHERE codigopdf = ?;";
        try {

            PreparedStatement ps = cone.getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                ObjPdf.setCodigopdf(rs.getInt(1));
                ObjPdf.setNombrepdf(rs.getString(2));
                ObjPdf.setArchivopdf2(rs.getBytes(3));
            }
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
        return ObjPdf;
    }

    // PARA  LOS PDF 2
    /*Metodo listar*/
    public ArrayList<Pdf> Listar_PdfVO_EV(String nivel_gp) {
        ArrayList<Pdf> list = new ArrayList<Pdf>();

        //String sql = "SELECT * FROM pdf;";
        String sql = "SELECT evidencias.codigopdf, evidencias.nombrepdf, evidencias.archivopdf from evidencias, estudiantes where (estudiantes.id_estudiantes = evidencias.id_estudiantes) and (estudiantes.nivel_gp = evidencias.nivel_gp) and evidencias.nivel_gp = '" + nivel_gp + "';";

        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Pdf objPdf = new Pdf();
                objPdf.setCodigopdf(rs.getInt(1));
                objPdf.setNombrepdf(rs.getString(2));
                objPdf.setArchivopdf2(rs.getBytes(3));
                list.add(objPdf);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }


    /*Metodo agregar*/
    public void Agregar_PdfVO_EV(Pdf objPdf) {

        String sql = "INSERT INTO evidencias (codigopdf, nombrepdf, archivopdf, nivel_gp, id_estudiantes) VALUES(?, ?, ?, ? ,?);";

        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);

            ps.setInt(1, objPdf.getCodigopdf());
            ps.setString(2, objPdf.getNombrepdf());
            ps.setBlob(3, objPdf.getArchivopdf());
            ps.setString(4, objPdf.getNivel_gp());
            ps.setInt(5, objPdf.getId_estudiantes());
            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }


    /*Metodo Modificar*/
    public void Modificar_PdfVO_EV(Pdf objPdf) {

        String sql = "UPDATE evidencias SET nombrepdf = ?, archivopdf = ? WHERE codigopdf = ?;";

        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);

            ps.setString(1, objPdf.getNombrepdf());
            ps.setBlob(2, objPdf.getArchivopdf());
            ps.setInt(3, objPdf.getCodigopdf());
            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /*Metodo Modificar*/
    public void Modificar_PdfVO2_EV(Pdf objPdf) {

        String sql = "UPDATE evidencias SET nombrepdf = ? WHERE codigopdf = ?;";

        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);

            ps.setString(1, objPdf.getNombrepdf());
            ps.setInt(2, objPdf.getCodigopdf());
            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /*Metodo Eliminar*/
    public void Eliminar_PdfVO_EV(int id) {

        String sql = "DELETE FROM evidencias WHERE codigopdf = ?;";

        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);

            ps.setInt(1, id);
            ps.executeUpdate();

        } catch (Exception ex) {
            System.err.println(ex);
        }
    }


    /*Metodo Consulta id*/
    public Pdf getPdfVOById_EV(int studentId) {
        Pdf ObjPdf = new Pdf();
        String sql = "SELECT * FROM evidencias WHERE codigopdf = ?;";
        try {

            PreparedStatement ps = cone.getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                ObjPdf.setCodigopdf(rs.getInt(1));
                ObjPdf.setNombrepdf(rs.getString(2));
                ObjPdf.setArchivopdf2(rs.getBytes(3));
            }
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
        return ObjPdf;
    }

    // PARA  LOS PDF 3
    /*Metodo listar*/
    public ArrayList<Pdf> Listar_PdfVO_IF(String nivel_gp) {
        ArrayList<Pdf> list = new ArrayList<Pdf>();

        //String sql = "SELECT * FROM pdf;";
        String sql = "SELECT informes.codigopdf, informes.nombrepdf, informes.archivopdf from informes, estudiantes where (estudiantes.id_estudiantes = informes.id_estudiantes) and (estudiantes.nivel_gp = informes.nivel_gp) and informes.nivel_gp = '" + nivel_gp + "';";

        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Pdf objPdf = new Pdf();
                objPdf.setCodigopdf(rs.getInt(1));
                objPdf.setNombrepdf(rs.getString(2));
                objPdf.setArchivopdf2(rs.getBytes(3));
                list.add(objPdf);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }


    /*Metodo agregar*/
    public void Agregar_PdfVO_IF(Pdf objPdf) {

        String sql = "INSERT INTO informes (codigopdf, nombrepdf, archivopdf, nivel_gp, id_estudiantes) VALUES(?, ?, ?, ? ,?);";

        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);

            ps.setInt(1, objPdf.getCodigopdf());
            ps.setString(2, objPdf.getNombrepdf());
            ps.setBlob(3, objPdf.getArchivopdf());
            ps.setString(4, objPdf.getNivel_gp());
            ps.setInt(5, objPdf.getId_estudiantes());
            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }


    /*Metodo Modificar*/
    public void Modificar_PdfVO_IF(Pdf objPdf) {

        String sql = "UPDATE informes SET nombrepdf = ?, archivopdf = ? WHERE codigopdf = ?;";

        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);

            ps.setString(1, objPdf.getNombrepdf());
            ps.setBlob(2, objPdf.getArchivopdf());
            ps.setInt(3, objPdf.getCodigopdf());
            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /*Metodo Modificar*/
    public void Modificar_PdfVO2_IF(Pdf objPdf) {

        String sql = "UPDATE informes SET nombrepdf = ? WHERE codigopdf = ?;";

        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);

            ps.setString(1, objPdf.getNombrepdf());
            ps.setInt(2, objPdf.getCodigopdf());
            ps.executeUpdate();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /*Metodo Eliminar*/
    public void Eliminar_PdfVO_IF(int id) {

        String sql = "DELETE FROM informes WHERE codigopdf = ?;";

        try {
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);

            ps.setInt(1, id);
            ps.executeUpdate();

        } catch (Exception ex) {
            System.err.println(ex);
        }
    }


    /*Metodo Consulta id*/
    public Pdf getPdfVOById_IF(int studentId) {
        Pdf ObjPdf = new Pdf();
        String sql = "SELECT * FROM informes WHERE codigopdf = ?;";
        try {

            PreparedStatement ps = cone.getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                ObjPdf.setCodigopdf(rs.getInt(1));
                ObjPdf.setNombrepdf(rs.getString(2));
                ObjPdf.setArchivopdf2(rs.getBytes(3));
            }
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
        return ObjPdf;
    }

    //CONSULTAS MODULO EMPRESA
    public ResultSet Listado() throws Exception {
        try {

            String sql = "SELECT * FROM EMPRESA";

            PreparedStatement ps = cone.getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            return rs;
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }
    }

    //Metodo para buscar por nombre de empresas los proyectos
    public ResultSet BuscarporNombre(String nombre) throws Exception {
        try {

            String sql = "SELECT * FROM EMPRESA WHERE (nombreEmpresa LIKE '" + nombre + "%')";
            PreparedStatement ps = cone.getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            return rs;
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }

    }
}
