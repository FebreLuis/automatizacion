-- MySQL dump 10.13  Distrib 5.7.24, for Win64 (x86_64)
--
-- Host: localhost    Database: utpl
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cartas_compromiso`
--

DROP TABLE IF EXISTS `cartas_compromiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cartas_compromiso` (
  `codigopdf` int(11) NOT NULL,
  `nombrepdf` text NOT NULL,
  `archivopdf` mediumblob NOT NULL,
  `nivel_gp` text NOT NULL,
  `id_estudiantes` int(11) NOT NULL,
  PRIMARY KEY (`codigopdf`),
  KEY `fk_estudi_cartas_idx` (`id_estudiantes`),
  CONSTRAINT `fk_estudi_cartas` FOREIGN KEY (`id_estudiantes`) REFERENCES `estudiantes` (`id_estudiantes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cartas_compromiso`
--

LOCK TABLES `cartas_compromiso` WRITE;
/*!40000 ALTER TABLE `cartas_compromiso` DISABLE KEYS */;
INSERT INTO `cartas_compromiso` VALUES (1,'Prueba',_binary 'e','2.2',1),(2,'Prueba',_binary 'e','3.1',2);
/*!40000 ALTER TABLE `cartas_compromiso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estudiantes`
--

DROP TABLE IF EXISTS `estudiantes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estudiantes` (
  `id_estudiantes` int(11) NOT NULL AUTO_INCREMENT,
  `cedula` text NOT NULL,
  `nombre` text NOT NULL,
  `apellido` text NOT NULL,
  `nivel_gp` text NOT NULL,
  `correo` text NOT NULL,
  `usuario` text NOT NULL,
  `pass` text NOT NULL,
  PRIMARY KEY (`id_estudiantes`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estudiantes`
--

LOCK TABLES `estudiantes` WRITE;
/*!40000 ALTER TABLE `estudiantes` DISABLE KEYS */;
INSERT INTO `estudiantes` VALUES (1,'1104127654','Luis Alfredo','Febre Paucar','2.2','lafebre@utpl.edu.ec','lafebre','luis123'),(2,'1104127860','Freddy Stalin','Villavicencio Espinoza','3.1','fsvillavicencio1@utpl.edu.ec','fvillavicencio','f123');
/*!40000 ALTER TABLE `estudiantes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyectos`
--

DROP TABLE IF EXISTS `proyectos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proyectos` (
  `id_proyectos` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_empresa` text NOT NULL,
  `nombre_de_dependencia` text NOT NULL,
  `nombre_del_encargado` text NOT NULL,
  `email` text NOT NULL,
  `telefono` text NOT NULL,
  `nombre_proyecto` text NOT NULL,
  `actividades` text NOT NULL,
  `nivel_gp` text NOT NULL,
  `estado` text NOT NULL,
  PRIMARY KEY (`id_proyectos`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyectos`
--

LOCK TABLES `proyectos` WRITE;
/*!40000 ALTER TABLE `proyectos` DISABLE KEYS */;
INSERT INTO `proyectos` VALUES (1,'UTPL','Dirección de las Tecnologías para la educación','Celia Paola Sarango Lapo','cpsarango@utpl.edu.edu.ec','0737014444','Mundos Virtuales','1.Levantamiento de requerimientos: Del 8 al 19 de octubre 2.Diseño de solución: Del 22 de octubre al 2 de noviembre 3.Implementación: del 5 de noviembre al 7 de enero 4.Pruebas del 9 de enero hasta el 31 de enero','2.2','Disponible'),(2,'UTPL','Titulación Sistemas Informaticas y Computación','Fernanda Soto','rlramirez@utpl.edu.ec','2522 ','Automatización de la gestión del prácticum.  Proyecto piloto: Titulación de Sistemas Informáticos y Computación','1. Levantamiento de requerimientos Del 8 al 19 de octubre \n2. Análisis y propuesta de solución  Del 22 al 31 de octubre\n3. Diseño de solución  Del 5 al 16 de noviembre \n4. Implementación  Del 19 de noviembre al 18 de enero\n 5. Pruebas Del 21 al 31 de enero','2.2','Disponible'),(3,'UTPL','TAW','Ramiro Ramirez','fmsoto@utpl.edu.ec','0991675654','Investigación, desarrollo e implementación de una herramienta de chat para la modalidad a distancia de la UTPL que mejore la enseñanza aprendizaje entre el docente y el estudiante','Investigación de trabajos relacionados en relación con chats educativos Investigar y proponer las características que deben tener el chat Generar un prototipo de chat educativo implementado en un componente de la modalidad abierta de la UTPL Proponer un método o proceso a implementar al momento de generar un componente académico','2.2','Ocupado'),(4,'UTPL','TAW','Ramiro Ramirez','rlramirez@utpl.edu.ec','0991675747','Desarrollo de una aplicación interactiva entre profesor – alumno para el uso de dispositivos móviles en el aula.','Levantamiento del arte previo Levantamiento de requisitos Desarrollo de un prototipo Implementación en el aula','2.2','Ocupado'),(5,'UTPL','TAW','Fernanda Soto','fmsoto@utpl.edu.ec','0991675654','Investigación, desarrollo e implementación de una herramienta de chat para la modalidad a distancia de la UTPL que mejore la enseñanza aprendizaje entre el docente y el estudiante','Levantamiento del arte previo Levantamiento de requisitos Desarrollo de un prototipo Implementación en el aula','2.2','Ocupado'),(6,'Proyecto de vinculación: RECITEC  Gestión de residuos computacionales','Departamento de Ciencias de la Computación y Electrónica','Segundo Benítez Hurtado','srbenitez@utpl.edu.ec','3701444','RECITEC – Gestión de residuos computacionales','1. Consolidación de información en torno a la gestión de residuos computacionales (1 al 31 de octubre) \n2. Propuesta de proceso para la gestión de residuos computacionales para comunidades (1 al 30 de noviembre) \n3. Propuesta de proceso para la gestión de residuos computacionales para los GADS (3 de diciembre de 2018 al 11 de enero de 2019) \n4. Informes finales de la gestión de residuos computacionales (  14 al 31 de enero de 2019)','3.1','Disponibles'),(7,'UTPL','TAW','Ramiro Ramirez','rlramirez@utpl.edu.ec','','Células culturales acoplado al proyecto de fototeca para la digitalización de los contenidos\nDesarrollo de software','Levantamiento de requerimientos\nDesarrollo de la solución de la células\nAcoplamiento al proyecto de fototeca\nVisualización de resultados de la aplicación web y móvil\nAplicación QR para la visualización de la información del portal','3.1','Disponible'),(8,'UTPL','TAW','Ramiro Ramírez','rlramirez@utpl.edu.ec','','Investigación, desarrollo e implementación de una herramienta de chat para la modalidad a distancia de la UTPL que mejore la enseñanza aprendizaje entre el docente y el estudiante. ','Investigación de trabajos relacionados en relación a chats educativos \nInvestigar y proponer las características que deben tener el chat \nGenerar un prototipo de chat educativo implementado en un componente de la modalidad abierta  de la UTPL\nProponer un método o proceso a implementar al momento de generar un componente académico. ','3.1','Disponible'),(9,'Grupo de investigación Control, Automation and Intelligent Systems','Departamento de Ciencias de la Computación y Electrónica. Sección Electrónica y Telecomunicaciones.','Carlos Alberto Calderón Córdova','cacalderon@utpl.edu.ec','07-3701444 ext: 2516','Desarrollo de una aplicación móvil Android, que gestione y controle un algoritmo de aprendizaje automático aplicado a imágenes capturadas por el Smartphone.\n(Miniaturización del prototipo del proyecto Axes)','1. Investigar el estado actual de librerías y APIs compatibles con Android, que permitan reconocer patrones basados en imágenes. Los patrones base son caracteres alfanuméricos. 8/oct – 26/oct.\n2. Diseñar el algoritmo, que incluya la interfaz, gestión y control de la librería/API que reconozca patrones alfanuméricos en imágenes. 29/oct – 23/nov.\n3. Desarrollar la aplicación móvil cuyo front-end es un juego lúdico para educación básica, basado en la identificación de números y/o letras. 26/nov – 14/dic.\n4. Evaluar el desempeño del aplicativo móvil, detección y corrección de errores. 17/dic – 1/feb.\n5. Elaboración de manual de programador de la aplicación móvil. 21/ene – 22/feb.\nNota: En el equipo de trabajo disponemos de los APIs para reconocer los patrones, sin embargo solamente los hemos integrado en Python. El presente trabajo es integrarlos en Android.','3.1','Disponible');
/*!40000 ALTER TABLE `proyectos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutor_academico`
--

DROP TABLE IF EXISTS `tutor_academico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tutor_academico` (
  `id_tutor_academico` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `apellido` varchar(60) NOT NULL,
  `cedula` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `paralelo` varchar(45) NOT NULL,
  `nivel_gp` double NOT NULL,
  `titulacion_departamento` varchar(60) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `pass` varchar(45) NOT NULL,
  PRIMARY KEY (`id_tutor_academico`),
  UNIQUE KEY `cedula_UNIQUE` (`cedula`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutor_academico`
--

LOCK TABLES `tutor_academico` WRITE;
/*!40000 ALTER TABLE `tutor_academico` DISABLE KEYS */;
INSERT INTO `tutor_academico` VALUES (1,'FERNANDA MARICELA',' SOTO GUERRERO','1142879635','fmsoto@utpl.edu.ec','0987541169','B',2.2,'DEPARTAMENTO DE CIENCIAS DE LA COMPUTACIÓN Y ELECTRÓNICA','sotof','s1234');
/*!40000 ALTER TABLE `tutor_academico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutor_externo`
--

DROP TABLE IF EXISTS `tutor_externo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tutor_externo` (
  `id_tutor_externo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `apellido` varchar(60) NOT NULL,
  `cedula` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `paralelo` varchar(45) NOT NULL,
  `nivel_gp` double NOT NULL,
  `titulacion_departamento` varchar(60) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `pass` varchar(45) NOT NULL,
  PRIMARY KEY (`id_tutor_externo`),
  UNIQUE KEY `cedula_UNIQUE` (`cedula`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutor_externo`
--

LOCK TABLES `tutor_externo` WRITE;
/*!40000 ALTER TABLE `tutor_externo` DISABLE KEYS */;
INSERT INTO `tutor_externo` VALUES (1,'OMAR ALEXANDER','RUÍZ VIVANCO','1154786355','noreply@nivel7.net','0988756321','A',2.2,'DEPARTAMENTO DE CIENCIAS DE LA COMPUTACIÓN Y ELECTRÓNICA','omarr','o12r');
/*!40000 ALTER TABLE `tutor_externo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'utpl'
--

--
-- Dumping routines for database 'utpl'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-20 13:16:39
